//
//  TodoTableViewController.swift
//  todoApp
//
//  Created by stakenoshita on 2019/04/03.
//  Copyright © 2019 stakenoshita. All rights reserved.
//

import UIKit

class TodoTableViewController: UITableViewController {
    // TODO: サーバーからデータを取得する
    private let todos = ["1", "2", "3"]

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return todos.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoTableViewCell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = todos[indexPath.row]
        
        return cell
    }

    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
