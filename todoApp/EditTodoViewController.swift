//
//  EditTodoViewController.swift
//  todoApp
//
//  Created by stakenoshita on 2019/04/04.
//  Copyright © 2019 stakenoshita. All rights reserved.
//

import UIKit

class EditTodoViewController: UIViewController {

    @IBOutlet weak private var titleTextField: UITextField!
    @IBOutlet weak private var detailTextView: UITextView!
    @IBOutlet weak private var dateTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    // MARK: - Navigation
    /*
    // In a storyboard-based application,
    // you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
